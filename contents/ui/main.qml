/*
 * This file is part of baseconverter-plasmoid.
 *
 * Baseconverter-plasmoid is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * baseconverter-plasmoid is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with baseconverter-plasmoid.If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2014 Zoltan Gyarmati <mr.zoltan.gyarmati@gmail.com>
 */
import QtQuick 1.0

Item {
    id: mainWindow

    // we can't bind the visible value to the config?!
    // doing this manually for now
    Component.onCompleted: {
        plasmoid.addEventListener("ConfigChanged", configChanged)
    }
    function configChanged (){
        binfield.visible =  plasmoid.readConfig("showbinary");
        octfield.visible =  plasmoid.readConfig("showoctal");
        decfield.visible =  plasmoid.readConfig("showdecimal");
        hexfield.visible =  plasmoid.readConfig("showhexadecimal");
    }
    Row {
        spacing: 5
        BaseLineEdit {
            id: binfield
            visible: plasmoid.readConfig("showbinary")
            helptext: 'Binary'
            base: 2
            onNumberChanged:{
                octfield.setNumber(val);
                decfield.setNumber(val);
                hexfield.setNumber(val);
            }
        }

        BaseLineEdit {
            id: octfield
            visible: plasmoid.readConfig("showoctal")
            helptext: 'Octal'
            base: 8
            onNumberChanged:{
                binfield.setNumber(val);
                decfield.setNumber(val);
                hexfield.setNumber(val);
            }
        }

        BaseLineEdit {
            id: decfield
            visible: plasmoid.readConfig("showdecimal")
            helptext: 'Decimal'
            base: 10
            onNumberChanged:{
                binfield.setNumber(val);
                octfield.setNumber(val);
                hexfield.setNumber(val);
            }
        }

        BaseLineEdit {
            id: hexfield
            visible: plasmoid.readConfig("showhexadecimal")
            helptext: 'Hexadecimal'
            base: 16
            onNumberChanged:{
                binfield.setNumber(val);
                octfield.setNumber(val);
                decfield.setNumber(val);
            }
        }
    }//row
}//mainwindow
