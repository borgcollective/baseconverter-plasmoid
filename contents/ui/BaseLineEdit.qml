/*
 * This file is part of baseconverter-plasmoid.
 *
 * Baseconverter-plasmoid is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * baseconverter-plasmoid is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with baseconverter-plasmoid.If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2014 Zoltan Gyarmati <mr.zoltan.gyarmati@gmail.com>
 */

import QtQuick 1.0
import org.kde.plasma.graphicswidgets 0.1 as PlasmaWidgets

Item {
    id: wrapper
    width: editfield.width
    height: editfield.height
    property int base: 10
    property alias helptext: editfield.clickMessage
    // emitted when the number was edited manually
    signal numberChanged(int val)
    //sets the value or "" as the text of the LineEdit
    function setNumber(val){
        if (val){
            editfield.text = n.toString(wrapper.base);
        }
        else{
            editfield.text ="";
        }
    }

    PlasmaWidgets.LineEdit {
        // it seems that activeFocus doesn't work with the
        // PlasmaWidgets::LineEdit, so we cache this value
        // TODO look into this
        property bool focused: false
        id: editfield
        onTextChanged:{
            if (editfield.focused){
                n = parseInt(text, wrapper.base) || 0;
                wrapper.numberChanged(n);
            }
        }
        onFocusChanged: {
            editfield.focused = focused;
        }
    }
}
